using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MapNodeData", menuName = "MapProject/MapNodeData", order = 0)]
public class MapNodeData : ScriptableObject 
{
    MapNodeType nodeType;
    
}

