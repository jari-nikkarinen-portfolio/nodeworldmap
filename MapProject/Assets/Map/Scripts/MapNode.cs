using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapNode 
{
    public int step;
    public int section;
    public Vector2 position;
    public MapNodeData data;
    public List<MapNode> connections;

}
