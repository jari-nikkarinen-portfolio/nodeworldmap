using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "MapNodeType", menuName = "MapProject/MapNodeType", order = 0)]
public class MapNodeType : ScriptableObject 
{
    public Color color = Color.white;
    public Texture2D texture;
    public float propability = 1f;  
}

