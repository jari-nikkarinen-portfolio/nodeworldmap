using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    public List<MapNode> nodes { get; } = new List<MapNode>();
}
